#include <environment\area.h>

Area::Area(Vector2 position) : m_position(position)
{

}

void Area::AddEntity(std::shared_ptr<goap::Entity> entity)
{
	m_entities.emplace_back(entity);
}

void Area::RemoveEntity(std::shared_ptr<goap::Entity> entity)
{
	auto it = std::find(m_entities.begin(), m_entities.end(), entity);
    if (it != m_entities.end()) { m_entities.erase(it); }
}

std::vector<std::shared_ptr<goap::Entity>> Area::Entities() const
{
	return m_entities;
}

const std::vector<std::shared_ptr<goap::Entity>>& Area::GetEntities() const
{
	return m_entities;
}

void Area::Disp(int n) const
{
	for (int i = 0; i < n; i++)
	{
		std::cout << " ";
	}
}
