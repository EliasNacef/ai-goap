#include <algorithm>

#include <environment\environment.h>
#include <goap\agent\agent.h>
#include <player\player.h>

Environment::Environment(int size) : m_size(size)
{
	for (int y = 0; y < size; y++)
	{
		for (int x = 0; x < size; x++)
		{
			Area area = Area(Vector2(x, y));
			m_areas.emplace_back(area);
		}
	}
}

void Environment::AddEntity(std::shared_ptr<goap::Entity> entity)
{
	const auto& pos = entity->GetPosition();
	if (!IsPositionValid(pos)) return;
	// Add in environment vector
	m_entities.emplace_back(entity);
	// Add in area vector
	auto& area = m_areas[pos.y * m_size + pos.x];
	area.AddEntity(entity);
}


void Environment::Tick()
{
	UpdateEntities();
}

bool Environment::IsPositionValid(const Vector2& position)
{
	for (auto& area : m_areas)
	{
		if (area.GetPosition() == position)
		{
			return true;
		}
	}
	return false;
}


void Environment::UpdateEntities()
{
	for (auto& area : m_areas)
	{
		for (auto& entity : area.Entities())
		{
			const auto& position = entity->GetPosition();
			// Behavior Update
			if (entity->GetType() == "Agent")
			{
				auto agent = dynamic_cast<goap::Agent*>(entity.get());
				if (agent->IsCatching())
				{
					agent->ResetVelocity();
					auto playerEntity = GetPlayerEntity(position, 1);
					if (playerEntity != nullptr)
					{
						auto player = dynamic_cast<Player*>(playerEntity.get());
						player->Catch();
					}
				}
			}

			// Move Update
			const auto& velocity = entity->GetVelocity();
			Vector2 nextPosition = position + velocity;
			bool isValid = IsPositionValid(nextPosition);
			if (isValid && (velocity.x != 0 || velocity.y != 0))
			{
				auto& currentArea = m_areas[position.y * m_size + position.x];
				currentArea.RemoveEntity(entity);
				MoveEntity(*entity, nextPosition);
				auto& nextArea = m_areas[nextPosition.y * m_size + nextPosition.x];
				nextArea.AddEntity(entity);
			}
		}
	}
}

void Environment::MoveEntity(goap::Entity& entity, const Vector2 nextPos)
{
	entity.SetPosition(nextPos);
	entity.ResetVelocity();
}

bool Environment::IsMouseCaught() const
{
	for (auto& entity : m_entities)
	{
		std::string type = entity->GetType();
		if (type == "Player")
		{
			auto player = dynamic_cast<Player*>(entity.get());
			return player->IsCaught();
		}
	}
	// No mouse
	return true;
}

std::shared_ptr<goap::Entity> Environment::GetPlayerEntity(const Vector2& position, int detectionRange) const
{
	auto xMin = std::clamp(position.x - detectionRange, 0, m_size - 1);
	auto xMax = std::clamp(position.x + detectionRange, 0, m_size - 1);
	auto yMin = std::clamp(position.y - detectionRange, 0, m_size - 1);
	auto yMax = std::clamp(position.y + detectionRange, 0, m_size - 1);

	std::vector<Area> aroundAreas;
	for (int y = yMin; y < yMax; y++)
	{
		for (int x = xMin; x < xMax; x++)
		{
			const auto area = m_areas[y * m_size + x];
			for (auto& entity : area.Entities())
			{
				std::string type = entity->GetType();
				if (type == "Player")
				{
					return entity;
				}
			}
		}
	}	
	return {};
}

void Environment::Disp()
{
	DispLine(Area::Size);
	for (int y = m_size - 1; y >= 0; y--)
	{
		for (int x = 0; x < m_size; x++)
		{
			std::cout << "|"; // Area separator

			auto& area = m_areas[y * m_size + x];
			const auto& areaPos = area.GetPosition();
			int nbAreaEntity = 0;
			for (auto entity : area.Entities())
			{
				const auto& entityPos = entity->GetPosition();
				if (areaPos.x == entityPos.x && areaPos.y == entityPos.y)
				{
					nbAreaEntity++;
					std::cout << entity->GetSymbol();
				}
			}
			area.Disp(Area::Size - nbAreaEntity);
		}
		std::cout << "|" << std::endl; // Close area line
		DispLine(Area::Size);
	}
}

void Environment::DispLine(int n) const
{
	for (int i = 0; i < m_size; i++)
	{
		std::cout << "+";
		for (int j = 0; j < n; j++)
		{
			std::cout << "-";
		}
	}
	std::cout << "+" << std::endl;
}
