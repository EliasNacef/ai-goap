#include <fstream>
#include <iostream>
#include <memory>
#include <thread>
#include <windows.h>
#include <chrono>
using namespace std::literals::chrono_literals;

#include <environment\environment.h>
#include <player\player.h>
#include <goap\agent\agent.h>
#include <goap\agent\agent_stats.h>
#include <goap\actions\goap_action.h>
#include <goap\actions\goap_action_catch_mouse.h>
#include <goap\goals\goap_goal.h>
#include <goap\json_deserializer.h>

#include <nlohmann\json.hpp>
using json = nlohmann::json;

void Tick(Environment& env, goap::Agent& agent)
{
    env.Tick();
    agent.Tick(env);

    // Disp
    system("CLS");
    env.Disp();
    std::cout << std::endl;
    agent.DispKnowledge();
    agent.DispCurrentAction();
    agent.DispPlan();
    std::this_thread::sleep_for(150ms);
}

void RunGame(std::shared_ptr<Environment> environment, std::shared_ptr<Player> mouse, std::shared_ptr<goap::Agent> agent)
{
    environment->Disp();
    while (true)
    {
        if (mouse->IsCaught())
        {
            std::cout << "Mouse has been caught !!" << std::endl;
            std::cout << "Game Over !" << std::endl;
            break;
        }
        if (GetAsyncKeyState(VK_UP))
        {
            // Move UP
            Vector2 direction(0, 1);
            mouse->MoveTowards(direction);
            Tick(*environment, *agent);
        }
        else if (GetAsyncKeyState(VK_DOWN))
        {
            // Move DOWN
            Vector2 direction(0, -1);
            mouse->MoveTowards(direction);
            Tick(*environment, *agent);
        }
        else if (GetAsyncKeyState(VK_RIGHT))
        {
            // Move RIGHT
            Vector2 direction(1, 0);
            mouse->MoveTowards(direction);
            Tick(*environment, *agent);
        }
        else if (GetAsyncKeyState(VK_LEFT))
        {
            // Move LEFT
            Vector2 direction(-1, 0);
            mouse->MoveTowards(direction);
            Tick(*environment, *agent);
        }
        else if (GetAsyncKeyState(VK_SPACE))
        {
            // Not Move
            Vector2 direction(0, 0);
            mouse->MoveTowards(direction);
            Tick(*environment, *agent);
        }
        else
        {
            // Not Move
            Vector2 direction(0, 0);
            mouse->MoveTowards(direction);
        }
    }
}

std::shared_ptr<goap::Agent> SetupAgent(json jActions, json jGoals)
{
    // Agent creation
    goap::AgentStats stats;
    stats.symbol = 'C';
    stats.energy = 10;
    stats.detectionRange = 3;
    auto agent = std::make_shared<goap::Agent>(1, stats);

    // Generate actions for agent
    auto actions = goap::GenerateActions(agent, jActions);
    std::cout << "ACTIONS : " << std::endl;
    for (auto& action : actions)
    {
        action->Disp();
    }
    std::cin.ignore();
    system("CLS");

    // Generate goals for agent
    auto goals = goap::GenerateGoals(jGoals);
    std::cout << "GOALS : " << std::endl;
    for (auto& goal : goals)
    {
        goal->Disp();
    }
    std::cin.ignore();
    system("CLS");

    // Assign available actions and goals to agent
    agent->SetAvailableActions(actions);
    agent->SetGoals(goals);

    return agent;
}

int main(int argc, char* argv[])
{
    if (argc != 3 || !goap::IsValidExtension(argv[1], ".json") || !goap::IsValidExtension(argv[2], ".json"))
    {
        std::cout << "The program need two arguments :" << std::endl;
        std::cout << "  - path to an agent available actions json file" << std::endl;
        std::cout << "  - path to an agent goals json file" << std::endl;
        std::cout << "example : .\\ai-goap ../data/goals.json ..data/actions.json" << std::endl;
    }
    else
    {
        // json deserializations
        json jActions = goap::Deserialize(argv[1]);
        json jGoals = goap::Deserialize(argv[2]);

        if(!goap::IsJsonActionsValid(jActions))
        {
            std::cout << "The agent available actions json file is not valid !" << std::endl;
        }
        else if(!goap::IsJsonGoalsValid(jGoals))
        {
            std::cout << "The agent goals json file is not valid !" << std::endl;
        }
        else
            // Json are valid !
        {
            // Environment generation
            auto environment = std::make_shared<Environment>(10);

            // Player Setup
            auto mouse = std::make_shared<Player>(0, 'M', Vector2(9 ,9));
            environment->AddEntity(mouse);

            // GOAP agent setup
            auto cat = SetupAgent(jActions, jGoals);
            environment->AddEntity(cat);

            // Run Game
            RunGame(environment, mouse, cat);

            std::cin.ignore();
        }
    }
    return EXIT_SUCCESS;
}