#include <goap\world_state.h>
#include "..\..\include\ai-goap\goap\world_state.h"

namespace goap
{
    WorldState::WorldState()
    {
    }

    void WorldState::AddProperty(std::string name, WorldProperty worldProperty)
    {
        auto it = m_properties.find(name);
        if (it != m_properties.end())
        {
            it->second = worldProperty;
        }
        else
        {
            m_properties.insert({ name, worldProperty });
        }
    }

    void WorldState::RemoveProperty(std::string name)
    {
        m_properties.erase(name);
    }

    const std::optional<WorldProperty> WorldState::GetProperty(std::string name) const
    {
        auto it = m_properties.find(name);
        if (it != m_properties.end())
        {
            return it->second;
        }
        else
        {
            return {};
        }
    }
}
