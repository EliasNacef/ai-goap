#include <goap/agent/agent_sensors.h>

namespace goap
{
    AgentSensors::AgentSensors()
    {}

    WorldState AgentSensors::Use(AgentStats stats, const Vector2& position, const Environment& env) const
    {
        WorldState wsSensed;
        WorldProperty tiredProperty;
        WorldProperty mouseFoundProperty;
        WorldProperty nearFoundProperty;
        WorldProperty mouseCaughtProperty;

        // Agent properties
        if (stats.energy > 0)
        {
            // Not tired
            tiredProperty.value = false;
        }
        else
        {
            // Tired
            tiredProperty.value = true;
        }

        // Mouse properties
        auto playerEntity = env.GetPlayerEntity(position, stats.detectionRange);
        if (playerEntity != nullptr)
        {
            Vector2 playerPosition = playerEntity->GetPosition();
            // Detected
            mouseFoundProperty.value = true;
            mouseFoundProperty.position = playerPosition;
            nearFoundProperty.position = playerPosition;
            if (position == playerPosition)
            {
                // Near
                nearFoundProperty.value = true;
            }
            else
            {
                // Not Near
                nearFoundProperty.value = false;
            }
        }
        else
        {
            // Not detected and not near
            mouseFoundProperty.value = false;
            nearFoundProperty.value = false;
        }

        // Mouse caught ?
        auto isMouseCaught = env.IsMouseCaught();
        if (isMouseCaught)
        {
            mouseCaughtProperty.value = true;
        }
        else
        {
            mouseCaughtProperty.value = false;
        }

        wsSensed.AddProperty("tired", tiredProperty);
        wsSensed.AddProperty("mouse_found", mouseFoundProperty);
        wsSensed.AddProperty("near_mouse", nearFoundProperty);
        wsSensed.AddProperty("mouse_caught", mouseCaughtProperty);

        return wsSensed;
    }
}