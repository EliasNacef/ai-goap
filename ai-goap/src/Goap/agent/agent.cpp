
#include <goap/agent/agent.h>
#include <goap/goals/goap_goal_catch_mouse.h>

namespace goap
{
	Agent::Agent(int id, AgentStats stats) :
		Entity(id),
		m_stats(stats),
		m_maxEnergy(stats.energy),
		m_isNewPlan(false),
		m_currentAction(nullptr)
	{}

	std::string Agent::GetType()
	{
		return "Agent";
	}

	char Agent::GetSymbol()
	{
		return m_stats.symbol;
	}

	void Agent::SetGoals(std::vector<std::shared_ptr<GoapGoal>> goals)
	{
		m_goals = goals;
	}

	void Agent::SetAvailableActions(std::vector<std::shared_ptr<GoapAction>> actions)
	{
		m_availableActions = actions;
	}

	void Agent::Tick(Environment& env)
	{
		// Reset action agent stats
		m_stats.isCatching = false;

		// Sense the current World State
		auto m_oldKnowledge = m_wsKnowledge;
		m_wsKnowledge = UseSensors(env);

		// Plan with GOAP if no action sequence in progress or knowledge has changed
		if (m_actionSequence.empty() || m_oldKnowledge != m_wsKnowledge)
		{
			m_isNewPlan = true;
			m_currentAction = nullptr;
			m_effectors.CancelAnimation();
			m_actionSequence = UseGoap(m_wsKnowledge);
		}

		// Try to execute the current plan
		UseEffectors(env);
	}

	WorldState Agent::UseSensors(const Environment& env)
	{
		return m_sensors.Use(m_stats, m_position, env);
	}

	std::deque<std::shared_ptr<GoapAction>> Agent::UseGoap(const WorldState ws)
	{
		std::sort(m_goals.begin(), m_goals.end(), [](const auto& lhs, const auto& rhs)
		{
			return lhs->GetPriority() < rhs->GetPriority();
		});

		for (const auto& goal : m_goals)
		{
			// Plan : try to find an action sequence to reach a goal
			auto actionSequence = m_planner.Plan(ws, *goal, m_availableActions);

			if (!actionSequence.empty())
				// An action sequence has been found
			{
				// Execute plan on ws copy and check if goal is satisfied at the end of this copy execution 
				if (IsPlanValid(ws, *goal, actionSequence))
					// Valid Plan
				{
					// Return this action sequence
					return actionSequence;
				}
				else
					// Invalid Plan
				{
					continue;
				}
			}
			else
				// No action sequence found
			{
				// This goal is not achievable for the moment
				continue;
			}
		}
		return {};
	}

	void Agent::UseEffectors(Environment& env)
	{
		if (m_effectors.IsFSMBusy() && !m_isNewPlan)
		{
			// Already an action in progress : tick effectors
			m_effectors.Tick();
		}
		else
		{
			// No action in progress, try to get another action from sequence
			if (m_actionSequence.empty()) return;
			m_isNewPlan = false;
			m_currentAction = m_actionSequence.front();
			m_actionSequence.pop_front();
			bool isPlanAlwaysValid = m_effectors.Use(env, m_wsKnowledge, m_currentAction);

			if (isPlanAlwaysValid)
			{
				// Tick the new action in progress
				m_effectors.Tick();
			}
			else
			{
				// Plan is no longer valid : previous action did not succeed to apply the expected effect
				// Force Replan with GOAP
				m_isNewPlan = true;
				m_currentAction = nullptr;
				m_effectors.CancelAnimation();
				m_actionSequence = UseGoap(m_wsKnowledge);
				UseEffectors(env);
			}
		}
	}

	void Agent::DispKnowledge() const
	{
		for (auto& property : m_wsKnowledge.GetProperties())
		{
			std::cout << "--> ";
			std::cout << property.first << std::endl;
			std::cout << "	value : ";
			if (property.second.value)
			{
				std::cout << "true" << std::endl;
			}
			else
			{
				std::cout << "false" << std::endl;
			}
		}
	}

	void Agent::DispPlan() const
	{
		std::cout << "GOAP Plan :" << std::endl;
		for (auto action : m_actionSequence)
		{
			action->DispName();
		}
	}

	void Agent::DispCurrentAction() const
	{
		std::cout << "Current Action :" << std::endl;
		if (m_currentAction)
		{
			m_currentAction->DispName();
		}
		else
		{
			std::cout << "NONE" << std::endl;
		}
	}

	void Agent::MoveTowards(Vector2 direction)
	{
		m_velocity.x = direction.x;
		m_velocity.y = direction.y;
	}

	bool Agent::IsPlanValid(WorldState worldState, const GoapGoal& goal, std::deque<std::shared_ptr<GoapAction>> actionSequence)
	{
		// Simulate an action sequence execution on worldstate
		for (const auto& action : actionSequence)
		{
			const auto& wsActionEffects = action->GetPostconditions();
			const auto& effectsProperties = wsActionEffects.GetProperties();
			for (auto& effectProperty : effectsProperties)
			{
				worldState.AddProperty(effectProperty.first, effectProperty.second);
			}
		}

		// Return goal checking
		return goal.IsGoalSatisfied(worldState);
	}
}