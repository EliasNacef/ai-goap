#include <goap/agent/agent_effectors.h>

namespace goap
{
	AgentEffectors::AgentEffectors() : m_currentState(nullptr)
	{

	}

	bool AgentEffectors::Use(Environment& env, WorldState& worldState, std::shared_ptr<GoapAction> action)
	{
		auto nextState = action->Perform(env, worldState);
		if (nextState != nullptr)
		{
			// Next state is valid
			m_currentState = nextState;
			return true;
		}
		else
		{
			// Problem with preconditions : replan needed
			return false;
		}
	}

	void AgentEffectors::Tick()
	{
		if (!m_currentState) return;
		m_currentState->Tick();
	}

	void AgentEffectors::CancelAnimation()
	{
		m_currentState = nullptr;
	}

	bool AgentEffectors::IsFSMBusy()
	{
		if (!m_currentState) return false;
		return !m_currentState->IsFinished();
	}
}