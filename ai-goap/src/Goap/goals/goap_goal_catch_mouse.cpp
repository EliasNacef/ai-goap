#include <goap\goals\goap_goal_catch_mouse.h>


namespace goap
{
	GoapGoalCatchMouse::GoapGoalCatchMouse(std::string name, int priority, const WorldState& worldState) :
		GoapGoal(name, priority, worldState)
	{}
}