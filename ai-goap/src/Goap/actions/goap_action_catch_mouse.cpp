#include <functional>

#include <goap\agent\agent.h>
#include <goap\actions\goap_action_catch_mouse.h>
#include <goap\states\animate.h>

namespace goap
{
	GoapActionCatchMouse::GoapActionCatchMouse(std::shared_ptr<Agent> agent, std::string name, int cost, WorldState preconditions, WorldState postconditions) :
		GoapAction(agent, name, cost, preconditions, postconditions)
	{}

	std::shared_ptr<State> GoapActionCatchMouse::ActivateAction(Environment& env, const WorldState& ws)
	{
		std::function<void()> lambdaAction = [&]() -> void
		{
			// Try to catch mouse
			std::cout << " Cat Catching Mouse !" << std::endl;
			m_agent->TryCatchMouse();
		};

		auto state = std::make_shared<AnimateState>(m_agent, lambdaAction);
		return state;
	}

	bool GoapActionCatchMouse::CheckProceduralPreconditions() const
	{
		return true;
	}
}