#include <goap/goap_planner.h>


namespace goap
{
	GoapPlanner::GoapPlanner()
	{
	}

	std::deque<std::shared_ptr<GoapAction>> GoapPlanner::Plan(const WorldState& currentWorldState, const GoapGoal& goal, std::vector<std::shared_ptr<GoapAction>> availableActions)
	{
		if (goal.IsGoalSatisfied(currentWorldState))
		{
			// Goal is already achieved
			return {};
		}

		auto& goalState = goal.GetGoal();

		// Find the best action sequence with A*
		const auto& actions = AStarSearch(currentWorldState, goalState, availableActions);

		return actions;
	}

	std::deque<std::shared_ptr<GoapAction>> GoapPlanner::AStarSearch(const WorldState& iniState, const WorldState& goalState, std::vector<std::shared_ptr<GoapAction>> availableGoalActions) const
	{
		// initial node
		auto start = std::make_shared<GoapNode>(goalState, availableGoalActions);

		// Open node list
		std::vector<std::shared_ptr<GoapNode>> open;

		// Close node list
		std::vector<std::shared_ptr<GoapNode>> close;

		//Goal node
		auto goal =  std::make_shared<GoapNode>(iniState, availableGoalActions);

		// Heuristic
		start->SetG(0);
		int hSuccessor = 0;
		auto successorProperties = start->GetWorldState().GetProperties();
		auto wsGoal = goal->GetWorldState();
		for (auto property : successorProperties)
		{
			const auto& oProperty = wsGoal.GetProperty(property.first);
			if (oProperty.has_value() && property.second.value != oProperty->value)
			{
				hSuccessor++;
			}
		}
		start->SetH(hSuccessor);
		start->SetF(start->GetG() + start->GetH());

		// No node currently selected
		std::shared_ptr<GoapNode> currentNode = nullptr;

		// The solution, list of action 
		std::deque<std::shared_ptr<GoapAction>> solution;

		//Put start in the open list and the existing nodes list
		open.emplace_back(start);

		while (!open.empty())
		{
			currentNode = nullptr;
			//Select the node with the lowest cost
			for (auto node : open) {
				if (currentNode == nullptr)
				{
					currentNode = node;
				}
				else if (currentNode->GetF() > node->GetF()) {
					currentNode = node;
				}
			}

			// remove current node from open list
			for (size_t nodeIndex = 0; nodeIndex < open.size(); nodeIndex++)
			{
				if (currentNode == open[nodeIndex])
				{
					open.erase(open.begin() + nodeIndex);
				}
			}

			// Check if the selected node state is the goal
			if (goal->GetWorldState().Validate(currentNode->GetWorldState())) {
				//for (auto node : close)
				//{
				//	solution.emplace_back(node->GetGeneratorAction());
				//}
				//std::reverse(solution.begin(), solution.end());
				auto backtracking = currentNode;
				while (backtracking != nullptr && backtracking->GetGeneratorAction() != nullptr)
				{
					solution.emplace_back(backtracking->GetGeneratorAction());
					backtracking = backtracking->GetParent();
				}
				return solution;
			}

			// add current node to close list
			close.emplace_back(currentNode);

			// Generate all current node's successors
			auto nodeSuccessors = currentNode->GenerateSuccessors(availableGoalActions);

			for (auto successor : nodeSuccessors)
			{
				// Calculate the cost from the start node to the succesor node
				int gSuccessor = currentNode->GetG() + successor->GetGeneratorAction()->GetCost();

				// Compute successor heuristic
				int hSuccessor = 0;
				auto successorProperties = successor->GetWorldState().GetProperties();
				auto goalProperties = goal->GetWorldState().GetProperties();				
				for (auto property : successorProperties)
				{
					if (property.second.value != goalProperties[property.first].value)
					{
						hSuccessor++;
					}
				}

				// Compute successor f function value
				int fSuccessor = gSuccessor + hSuccessor;

				// 
				bool isInOpen = false;

				for (auto node : open)
				{
					if (successor == node)
					{
						// successor is in open list
						if (fSuccessor > node->GetF())
						{
							// successor has a better f value
							continue; // go to next successor
						}
					}
				}

				for (auto node : close)
				{
					if (successor == node)
					{
						// successor is in close list
						if (fSuccessor > node->GetF())
						{
							// successor has a better f value
							continue; // go to next successor
						}
					}
				}

				successor->SetParent(currentNode);
				successor->SetG(gSuccessor);
				successor->SetH(hSuccessor);
				successor->SetF(fSuccessor);

				auto itOpen = std::find(open.begin(), open.end(), successor);
				if (itOpen != open.end()) { open.erase(itOpen); }
				auto itClose = std::find(close.begin(), close.end(), successor);
				if (itClose != close.end()) { close.erase(itClose); }
				open.emplace_back(successor);
			}
		}
		std::reverse(solution.begin(), solution.end());
		return solution;
	}
}

