#include <iostream>
#include <goap\goals\goap_goal.h>

namespace goap
{
	GoapGoal::GoapGoal(std::string name, int priority, const WorldState& worldState) :
		m_name(name),
		m_goal(worldState),
		m_priority(priority)
	{}


	bool GoapGoal::IsReplanRequired() const
	{
		// TODO
		return false;
	}

	bool GoapGoal::IsGoalSatisfied(const WorldState& ws) const
	{
		const auto& wsProperties = ws.GetProperties();
		for (const auto& goalProperty : m_goal.GetProperties())
		{
			auto it = wsProperties.find(goalProperty.first);
			if (it != wsProperties.end())
			{
				if (goalProperty.second.value != it->second.value)
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		return true;
	}

	const WorldState& GoapGoal::GetGoal() const
	{
		return m_goal;
	}

	int GoapGoal::GetPriority() const
	{
		return m_priority;
	}

	void GoapGoal::Disp() const
	{
		std::cout << "--> ";
		std::cout << m_name << std::endl;
		std::cout << "	priority: " << m_priority << std::endl;

		std::cout << "	goal: " << std::endl;
		const auto& properties = m_goal.GetProperties();
		for (auto& property : properties)
		{
			std::cout << "		" << property.first << " : ";
			if (property.second.value)
			{
				std::cout << "true" << std::endl;
			}
			else
			{
				std::cout << "false" << std::endl;
			}
		}
	}
}


