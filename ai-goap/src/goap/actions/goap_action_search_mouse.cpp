#include <functional>

#include <goap\actions\goap_action_search_mouse.h>
#include <goap\states\go_to.h>

namespace goap
{
	GoapActionSearchMouse::GoapActionSearchMouse(std::shared_ptr<Agent> agent, std::string name, int cost, WorldState preconditions, WorldState postconditions) :
		GoapAction(agent, name, cost, preconditions, postconditions)
	{}

	std::shared_ptr<State> GoapActionSearchMouse::ActivateAction(Environment& env, const WorldState& ws)
	{
		//  State setup
		int envSize = env.GetSize();
		Vector2 destination = Vector2(std::rand() % envSize, std::rand() % envSize);
		std::function<void()> lambdaAction = [&]() -> void
		{
			// DO NOTHING : JUST GOTO ACTION
		};

		auto state = std::make_shared<GoToState>(m_agent, lambdaAction, destination);
		return state;
	}

	bool GoapActionSearchMouse::CheckProceduralPreconditions() const
	{
		return true;
	}
}