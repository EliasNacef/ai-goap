#include <functional>

#include <goap\actions\goap_action_sleep.h>
#include <goap\agent\agent.h>
#include <goap\states\animate.h>

namespace goap
{
	GoapActionSleep::GoapActionSleep(std::shared_ptr<Agent> agent, std::string name, int cost, WorldState preconditions, WorldState postconditions) :
		GoapAction(agent, name, cost, preconditions, postconditions)
	{}

	std::shared_ptr<State> GoapActionSleep::ActivateAction(Environment& env, const WorldState& ws)
	{
		//  State setup
		std::function<void()> lambdaAction = [&]() -> void
		{
			// Sleep
			int maxEnergy = m_agent->GetMaxEnergy();
			m_agent->SetEnergy(maxEnergy);
		};

		auto state = std::make_shared<AnimateState>(m_agent, lambdaAction);
		return state;
	}

	bool GoapActionSleep::CheckProceduralPreconditions() const
	{
		return true;
	}
}