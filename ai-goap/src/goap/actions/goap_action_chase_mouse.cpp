#include <functional>

#include <goap\actions\goap_action_chase_mouse.h>
#include <goap\states\go_to.h>

namespace goap
{
	GoapActionChaseMouse::GoapActionChaseMouse(std::shared_ptr<Agent> agent, std::string name, int cost, WorldState preconditions, WorldState postconditions) :
		GoapAction(agent, name, cost, preconditions, postconditions)
	{}

	std::shared_ptr<State> GoapActionChaseMouse::ActivateAction(Environment& env, const WorldState& ws)
	{
		//  State setup
		Vector2 destination;
		const auto& property = ws.GetProperty("mouse_found");
		if (property.has_value())
		{
			destination = property->position;
		}
		std::function<void()> lambdaAction = [&]() -> void
		{
			// DO NOTHING : JUST GOTO ACTION
		};

		auto state = std::make_shared<GoToState>(m_agent, lambdaAction, destination);
		return state;
	}

	bool GoapActionChaseMouse::CheckProceduralPreconditions() const
	{
		return true;
	}
}