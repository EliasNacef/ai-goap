#include <functional>

#include <goap\states\animate.h>
#include <goap\actions\goap_action.h>

namespace goap 
{
	GoapAction::GoapAction(std::shared_ptr<Agent> agent, std::string name, int cost, WorldState preconditions, WorldState postconditions) :
		m_agent(agent),
		m_name(name),
		m_cost(cost),
		m_preconditions(preconditions),
		m_postconditions(postconditions)
	{}

	std::shared_ptr<State> GoapAction::Perform(Environment& env, const WorldState& ws)
	{
		if (CheckPreconditions(ws)
			&& CheckProceduralPreconditions())
		{
			return ActivateAction(env, ws);
		}
		else
		{
			return nullptr;
		}
	}

	std::shared_ptr<State> GoapAction::ActivateAction(Environment& env, const WorldState& ws)
	{
		std::function<void()> lambdaAction = [&]() -> void
		{
			// EMPTY ACTION : DO NOTHING
		};

		auto state = std::make_shared<AnimateState>(m_agent, lambdaAction); // static state for base class
		return state;
	}

	bool GoapAction::CheckPreconditions(const WorldState& ws) const
	{
		const auto& wsProperties = ws.GetProperties();
		const auto& preActionProperties = m_preconditions.GetProperties();

		for (const auto& property : preActionProperties)
		{
			auto it = wsProperties.find(property.first);
			if (it != wsProperties.end())
			{
				if (property.second.value != it->second.value)
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		return true;
	}

	bool GoapAction::CheckProceduralPreconditions() const
	{
		return true;
	}

	void GoapAction::DispName() const
	{
		std::cout << m_name << std::endl;
	}

	void GoapAction::Disp() const
	{
		std::cout << "--> ";
		std::cout << m_name << std::endl;
		std::cout << "	cost: " << m_cost << std::endl;

		std::cout << "	preconditions: " << std::endl;
		const auto& preProperties = m_preconditions.GetProperties();
		for (auto& property : preProperties)
		{
			std::cout << "		" << property.first << " : ";
			if (property.second.value)
			{
				std::cout << "true" << std::endl;
			}
			else
			{
				std::cout << "false" << std::endl;
			}
		}

		std::cout << "	postconditions: " << std::endl;
		const auto& postProperties = m_postconditions.GetProperties();
		for (auto& property : postProperties)
		{
			std::cout << "		" << property.first << " : ";
			if (property.second.value)
			{
				std::cout << "true" << std::endl;
			}
			else
			{
				std::cout << "false" << std::endl;
			}
		}
	}
}