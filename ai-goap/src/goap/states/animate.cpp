#include <goap\states\animate.h>

namespace goap
{
	AnimateState::AnimateState(std::shared_ptr<Agent> agent, std::function<void()> lambdaAction) :
		State(agent, lambdaAction)
	{}

	void AnimateState::Tick()
	{
		if (m_phase == StatePhase::START) m_phase = StatePhase::IN_PROGRESS;
		if (m_phase == StatePhase::EXIT) return;
		m_lambdaAction();
		m_phase = StatePhase::EXIT;
	}
}