#include <goap\states\go_to.h>
#include <goap\agent\agent.h>

namespace goap
{
	GoToState::GoToState(std::shared_ptr<Agent> agent, std::function<void()> lambdaAction, Vector2 destination) :
		State(agent,lambdaAction),
		m_destination(destination)
	{
	}

	void GoToState::Tick()
	{
		if (m_phase == StatePhase::START) m_phase = StatePhase::IN_PROGRESS;
		if (m_phase == StatePhase::EXIT) return;

		auto position = m_agent->GetPosition();
		if (position != m_destination)
		{
			auto energy = m_agent->GetEnergy();
			if (energy > 0)
			{
				// Move towards action place
				auto direction = (m_destination - position).Normalize2D();
				m_agent->MoveTowards(direction);
				m_agent->SetEnergy(energy - 1);
			}
			else
			{
				// Sleep
				m_agent->SetEnergy(m_agent->GetMaxEnergy());
			}
		}
		else
		{
			m_lambdaAction();
			m_phase = StatePhase::EXIT;
		}

	}
}