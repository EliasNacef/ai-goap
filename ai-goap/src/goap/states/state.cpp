#include <goap/states/state.h>

namespace goap
{
	State::State(std::shared_ptr<Agent> agent, std::function<void()> lambdaAction) :
		m_phase(StatePhase::START),
		m_agent(agent),
		m_lambdaAction(lambdaAction)
	{}

	void State::Tick()
	{
		if(m_phase == StatePhase::START) m_phase = StatePhase::IN_PROGRESS;
		if(m_phase == StatePhase::EXIT) return;
		m_lambdaAction();
		m_phase = StatePhase::EXIT;
	}

	bool State::IsFinished()
	{
		return (m_phase == StatePhase::EXIT);
	}
}