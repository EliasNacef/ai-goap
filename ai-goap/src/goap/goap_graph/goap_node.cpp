#include <goap\graph\goap_node.h>

namespace goap
{
	GoapNode::GoapNode(WorldState worldState, std::vector<std::shared_ptr<goap::GoapAction>> actions): m_ws(worldState)
	{
		m_parent = nullptr;
		//Add the action that the agent can made with the node world state
		std::vector<std::shared_ptr<goap::GoapAction>> possibleActions;
		for (auto& action : actions) {
			const auto& postconditions = action->GetPostconditions();
			if (postconditions.ValidatePartially(m_ws))
			{
				possibleActions.emplace_back(action);
			}
		}
		m_actions = possibleActions;

		// Set the g function value at 
		m_g = 0;
		m_h = 0;
		m_f = 0;
	}

	std::shared_ptr<GoapNode> GoapNode::GetParent()
	{
		return m_parent;
	}

	void GoapNode::SetParent(std::shared_ptr<GoapNode> parent)
	{
		m_parent = parent;
	}

	std::shared_ptr<GoapAction> GoapNode::GetGeneratorAction()
	{
		return m_generatorAction;
	}

	void GoapNode::SetGeneratorAction(std::shared_ptr<GoapAction> action)
	{
		m_generatorAction = action;
	}

	WorldState GoapNode::GetWorldState() 
	{
		return m_ws;
	}

	std::vector<std::shared_ptr<goap::GoapAction>> GoapNode::GetActions() 
	{
		return m_actions;
	}

	int GoapNode::GetG() 
	{
		return m_g;
	}

	void GoapNode::SetG(int g) 
	{
		m_g = g;
	}

	int GoapNode::GetH()
	{
		return m_h;
	}

	void GoapNode::SetH(int h)
	{
		m_h = h;
	}

	int GoapNode::GetF()
	{
		return m_f;
	}

	void GoapNode::SetF(int f)
	{
		m_f = f;
	}

	std::vector<std::shared_ptr<GoapNode>> GoapNode::GenerateSuccessors(std::vector<std::shared_ptr<goap::GoapAction>> actions)
	{
		// The vector to be retun with the successors
		std::vector<std::shared_ptr<GoapNode>> successors;

		//For each action it generate a successor
		for (auto& action : m_actions) {
			const auto& postconditions = action->GetPostconditions();
			if (postconditions.ValidatePartially(m_ws))
			{
				auto successorNode = std::make_shared<GoapNode>(action->GetPreconditions(), actions);
				successorNode->SetGeneratorAction(action);
				successors.emplace_back(successorNode);
			}
		}
		return successors;
	}
}

