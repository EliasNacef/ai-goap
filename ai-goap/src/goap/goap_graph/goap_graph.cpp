#include <goap/graph/goap_graph.h>

#include <algorithm>

namespace goap
{
	GoapGraph::GoapGraph(std::vector<std::shared_ptr<GoapAction>> actions)
	{
		for (auto& action : actions)
		{
			AddNode(action);
		}
	}

	void GoapGraph::AddNode(std::shared_ptr<GoapAction> action)
	{
		const auto& preState = action->GetPreconditions();

		auto it = std::find_if(m_graph.begin(), m_graph.end(), [&](GoapNode& node) { return node.GetWorldState() == preState; });
		if (it->GetWorldState().Validate(m_graph.end()->GetWorldState()))
		{
			// Not found
			GoapNode node;
			node.GetWorldState() = preState;
			node.SetGeneratorAction(action);
			m_graph.emplace_back(node);
		}
		else
		{
			it->SetGeneratorAction(action);
		}
	}
}