#include <player/player.h>

Player::Player(int id, char symbol) :
	Entity(id, Vector2(0, 0)),
	m_symbol(symbol),
	m_isCaught(false)
{
}

Player::Player(int id, char symbol, Vector2 position) :
	Entity(id, position),
	m_symbol(symbol),
	m_isCaught(false)
{}

std::string Player::GetType()
{
	return "Player";
}

char Player::GetSymbol()
{
	return m_symbol;
}

void Player::MoveTowards(Vector2 direction)
{
	m_velocity.x = direction.x;
	m_velocity.y = direction.y;
}

void Player::Catch()
{
	m_isCaught = true;
}

bool Player::IsCaught()
{
	return m_isCaught;
}
