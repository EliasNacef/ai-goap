#pragma once

#include <goap/components/entity.h>
#include <goap/components/vector2.h>

class Player : public goap::Entity
{
public:
	// ctors
	Player(int id, char symbol);
	Player(int id, char symbol, Vector2 position);
	Player(const Player&) = delete;
	Player(Player&&) = delete;

	/// <summary>
	/// Return entity type
	/// </summary>
	/// <returns> string type </returns>
	std::string GetType() override;

	/// <summary>
	/// Return char representation of a player
	/// </summary>
	/// <returns> Char representation </returns>
	char GetSymbol() override;

	void MoveTowards(Vector2 direction);

	void Catch();

	bool IsCaught();

private:
	char m_symbol;
	bool m_isCaught;
};