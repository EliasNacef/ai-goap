#pragma once

#include <vector>
#include <optional>

#include <environment/area.h>
#include <goap/components/vector2.h>
#include <goap/components/entity.h>

/// <summary>
/// Define environment
/// </summary>
class Environment
{
public:

	/// <summary>
	/// Ctor 
	/// </summary>
	Environment(int size);

	// No copy ctor
	Environment(const Environment&) = delete;
	// No move ctor
	Environment(Environment&&) = delete;

	void AddEntity(std::shared_ptr<goap::Entity> entity);

	/// <summary>
	/// Environment update one time
	/// </summary>
	void Tick();

	bool IsPositionValid(const Vector2& position);

	void UpdateEntities();

	void MoveEntity(goap::Entity& entity, const Vector2 nextPos);

	bool IsMouseCaught() const;

	std::shared_ptr<goap::Entity> GetPlayerEntity(const Vector2& position, int detectionRange) const;

	/// <summary>
	/// Get environment size
	/// </summary>
	/// <returns></returns>
	size_t GetSize() const
	{
		return m_size;
	}

	/// <summary>
	/// Disp the environment in console
	/// </summary>
	void Disp();

private:
	int m_size;
	std::vector<Area> m_areas;
	std::vector<std::shared_ptr<goap::Entity>> m_entities;
	void DispLine(int n) const;
};