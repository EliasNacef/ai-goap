#pragma once

#include <vector>

#include <goap/components/vector2.h>
#include <goap/components/entity.h>

/// <summary>
/// Define an area in an environment
/// </summary>
class Area
{
public:
	const static int Size = 3;

	/// <summary>
	/// ctor
	/// </summary>
	/// <param name="position"> area position </param>
	Area(Vector2 position);

	/// <summary>
	/// Copy ctor
	/// </summary>
	Area(const Area&) = default;

	void AddEntity(std::shared_ptr<goap::Entity> entity);

	void RemoveEntity(std::shared_ptr<goap::Entity> entity);

	std::vector<std::shared_ptr<goap::Entity>> Entities() const;
	const std::vector<std::shared_ptr<goap::Entity>>& GetEntities() const;

	/// <summary>
	/// Getter for area position
	/// </summary>
	/// <returns> area position </returns>
	const Vector2& GetPosition() const
	{
		return m_position;
	}

	/// <summary>
	/// Disp the area in console
	/// </summary>
	void Disp(int n) const;

private:
	// Position
	Vector2 m_position;
	std::vector<std::shared_ptr<goap::Entity>> m_entities;
};