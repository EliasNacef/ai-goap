#pragma once

#include <goap/states/state.h>
#include <goap/world_state.h>

namespace goap
{
	/// <summary>
	/// GOAP Action
	/// </summary>
	class GoapAction
	{
	public:

		GoapAction(std::shared_ptr<Agent> agent, std::string name, int cost, WorldState preconditions, WorldState postconditions);

		/// <summary>
		/// Check action and activate it if possible
		/// </summary>
		/// <param name=""></param>
		std::shared_ptr<State> Perform(Environment& env, const WorldState& ws);
		
		/// <summary>
		/// Activate the checked action :
		/// Tell the FSM what is the next state
		/// Give some parameters to the FSM for next state
		/// </summary>
		virtual std::shared_ptr<State> ActivateAction(Environment& env, const WorldState& ws);

		/// <summary>
		/// Check World State Preconditions
		/// </summary>
		/// <returns></returns>
		bool CheckPreconditions(const WorldState& state) const;

		/// <summary>
		/// Check context preconditions
		/// </summary>
		/// <returns></returns>
		virtual bool CheckProceduralPreconditions() const;

		int GetCost() const
		{
			return m_cost;
		}

		const WorldState& GetPreconditions() const
		{
			return m_preconditions;
		}

		const WorldState& GetPostconditions() const
		{
			return m_postconditions;
		}

		void DispName() const;
		void Disp() const;

	protected:
		// Agent
		std::shared_ptr<Agent> m_agent;
		// Action name
		std::string m_name;
		// Action cost
		int m_cost;
		// Precondition
		WorldState m_preconditions;
		// Effect
		WorldState m_postconditions;
	};
}
