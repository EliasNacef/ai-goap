#pragma once

#include <goap/actions/goap_action.h>

namespace goap
{
	/// <summary>
	/// GOAP Action to search mouse
	/// </summary>
	class GoapActionSearchMouse : public GoapAction
	{
	public:

		GoapActionSearchMouse(std::shared_ptr<Agent> agent, std::string name, int cost, WorldState preconditions, WorldState postconditions);

		/// <summary>
		/// Activate the checked action :
		/// Tell the FSM what is the next state
		/// Give some parameters to the FSM for next state
		/// </summary>
		std::shared_ptr<State> ActivateAction(Environment& env, const WorldState& ws) override;

		bool CheckProceduralPreconditions() const override;
	};
}