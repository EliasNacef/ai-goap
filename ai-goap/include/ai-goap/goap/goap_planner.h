#pragma once

#include <iostream>
#include <queue>

#include <goap/actions/goap_action.h>
#include <goap/goals/goap_goal.h>
#include <goap/graph/goap_node.h>

namespace goap
{
	/// <summary>
	/// Goal-Oriented Action Planner (GOAP)
	/// </summary>
	class GoapPlanner
	{
	public:

		GoapPlanner();

		std::deque<std::shared_ptr<GoapAction>> Plan(const WorldState& currentWorldState, const GoapGoal& goal, std::vector<std::shared_ptr<GoapAction>> availableActions);

	private:
		std::deque<std::shared_ptr<GoapAction>> AStarSearch(const WorldState& iniState, const WorldState& goalState, std::vector<std::shared_ptr<GoapAction>> availableAction) const;
	};
}
