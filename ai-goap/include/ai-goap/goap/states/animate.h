#pragma once

#include <goap\states\state.h>

namespace goap
{
	/// <summary>
	/// Occurs with a static animation
	/// </summary>
	class AnimateState : public State
	{
	public:
		AnimateState(std::shared_ptr<Agent> agent, std::function<void()> lambdaAction);

		void Tick() override;
	};
}