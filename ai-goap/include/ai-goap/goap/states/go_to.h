#pragma once

#include <goap\components\vector2.h>
#include <goap\states\state.h>

namespace goap
{
	/// <summary>
	/// Occurs with a movement animation
	/// </summary>
	class GoToState : public State
	{
	public:
		GoToState(std::shared_ptr<Agent> agent, std::function<void()> lambdaAction, Vector2 destination);

		void Tick() override;

	private:
		Vector2 m_destination;
	};
}