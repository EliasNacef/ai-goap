#pragma once

#include <iostream>
#include <functional>

#include <environment\environment.h>
#include <goap\world_state.h>


namespace goap
{
	class Agent;

	enum class StatePhase
	{
		START, IN_PROGRESS, EXIT
	};

	class State
	{
	public:
		State(std::shared_ptr<Agent> m_agent, std::function<void()>);

		virtual void Tick();

		bool IsFinished();

	protected:
		StatePhase m_phase;
		std::shared_ptr<Agent> m_agent;
		std::function<void()> m_lambdaAction;
	};
}