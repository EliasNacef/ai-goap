#pragma once

#include <goap/components/vector2.h>

namespace goap
{
	class Entity
	{
	public:
		Entity(int id) : m_id(id) {}
		Entity(int id, Vector2 position) : m_id(id), m_position(position) {}

		/// <summary>
		/// Return entity type
		/// </summary>
		/// <returns> string type </returns>
		virtual std::string GetType()
		{
			return "Entity";
		}

		/// <summary>
		/// Return char representation of an entity
		/// </summary>
		/// <returns> Char representation </returns>
		virtual char GetSymbol()
		{
			return '?';
		}

		void SetPosition(const Vector2& newPosition)
		{
			m_position.x = newPosition.x;
			m_position.y = newPosition.y;
		}

		void ResetVelocity()
		{
			m_velocity.x = 0;
			m_velocity.y = 0;
		}

		int GetID() const
		{
			return m_id;
		}

		const Vector2& GetPosition() const
		{
			return m_position;
		}

		const Vector2& GetVelocity() const
		{
			return m_velocity;
		}

	protected:
		int m_id;
		Vector2 m_position;
		Vector2 m_velocity;
	};
}