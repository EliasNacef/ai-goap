#pragma once
#include <iostream>
#include <string>
#include <cmath>  

struct Vector2
{
	int x;
	int y;

	Vector2() : x(0), y(0) {}
    Vector2(int xVal, int yVal) : x(xVal), y(yVal) {}

    Vector2 operator+(const Vector2& p) const
    {
        Vector2 v2;
        v2.x = x + p.x;
        v2.y = y + p.y;
        return v2;
    }

    Vector2 operator-(const Vector2& p) const
    {
        Vector2 v2;
        v2.x = x - p.x;
        v2.y = y - p.y;
        return v2;
    }

	bool operator==(const Vector2& p) const
    {
        return (x == p.x && y == p.y);
    }

    bool operator!=(const Vector2& p) const
    {
        return (x != p.x || y != p.y);
    }

    Vector2 Normalize2D() const
    {
        if (x == 0 && y == 0) return Vector2(0, 0);
        if (abs(x) > abs(y))
        {
            // x is predominant
            if (x > 0)
            {
                return Vector2(1, 0);
            }
            else
            {
                return Vector2(-1, 0);
            }
        }
        else
        {
            if (y > 0)
            {
                return Vector2(0, 1);
            }
            else
            {
                return Vector2(0, -1);
            }
        }
    }

	std::string ToString() const
    {
        return "(x = " + std::to_string(x) + ", y = " + std::to_string(y) + ")";
    }
};

