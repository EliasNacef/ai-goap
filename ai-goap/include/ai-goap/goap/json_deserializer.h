#pragma once

#include <iostream>
#include <fstream>
#include <goap\actions\goap_action.h>
#include <goap\actions\goap_action_catch_mouse.h>
#include <goap\actions\goap_action_chase_mouse.h>
#include <goap\actions\goap_action_search_mouse.h>
#include <goap\actions\goap_action_sleep.h>
#include <goap\goals\goap_goal.h>
#include <goap\goals\goap_goal_catch_mouse.h>

#include <nlohmann\json.hpp>
using json = nlohmann::json;

namespace goap
{
    /// <summary>
    /// Deserialize json file and return the matching json object
    /// </summary>
    /// <param name="jsonPath"> json file path </param>
    /// <returns> json object corresponding </returns>
    json Deserialize(std::string jsonPath)
    {
        std::ifstream i(jsonPath);
        json j;
        i >> j;
        return j;
    }

    /// <summary>
    /// Check the file extension : return true if the file extension is matching
    /// </summary>
    /// <param name="filePath"> file path to check </param>
    /// <param name="suffix"> expected extension </param>
    /// <returns> true / false </returns>
    inline bool IsValidExtension(std::string const& filePath, std::string const& suffix)
    {
        if (suffix.size() > filePath.size()) return false;
        return std::equal(suffix.rbegin(), suffix.rend(), filePath.rbegin());
    }

    /// <summary>
    /// Check if the json object is matching with an expected available actions json object
    /// </summary>
    /// <param name="j"> json object </param>
    /// <returns> true / false </returns>
    bool IsJsonActionsValid(json& j)
    {
        // check actions json validity
        if (j.empty()) return false; // no entry
        if (j.size() != 1) return false; // must exactly have 1 entry
        if (!j.is_object()) return false; // must be a json object
        if (!j.contains("actions")) return false; // the root object name is 'actions'

        for (auto it = j["actions"].begin(); it != j["actions"].end(); ++it)
        {
            if (!it->contains("cost") || !it->find("cost")->is_number()) return false; // an action must have a 'cost' int
            if (!it->contains("preconditions")) return false; // an action must have a 'preconditions' object
            if (!it->contains("postconditions")) return false; // an action must have a 'postconditions' object
        }

        // available actions json file is valid
        return true;
    }


    /// <summary>
    /// Check if the json object is matching with an expected goals json object
    /// </summary>
    /// <param name="j"> json object </param>
    /// <returns> true / false </returns>
    bool IsJsonGoalsValid(json& j)
    {
        // check goals json validity
        if (j.empty()) return false; // no entry
        if (j.size() != 1) return false; // must have 1 entry
        if (!j.is_object()) return false; // must be a json object
        if (!j.contains("goals")) return false; // the root object name is 'goals'

        for (auto it = j["goals"].begin(); it != j["goals"].end(); ++it)
        {
            if (!it->contains("priority") || !it->find("priority")->is_number()) return false; // a goal must have a 'priority' int
            if (!it->contains("goal")) return false; // a goal must have a 'goal' object
        }

        // goals json file is valid
        return true;
    }


    /// <summary>
    /// Generate available actions according to the corresponding json object and return them in a vector
    /// </summary>
    /// <param name="j"> available actions json object </param>
    /// <returns> actions </returns>
    std::vector<std::shared_ptr<goap::GoapAction>> GenerateActions(std::shared_ptr<Agent> agent, json& j)
    {
        std::vector<std::shared_ptr<goap::GoapAction>> actions;
        for (const auto& action : j["actions"].items())
        {
            // Find out action cost
            int cost = action.value()["cost"].get<int>();

            // Find out preconditions
            auto preconditions = WorldState();
            for (const auto& precond : action.value()["preconditions"].items())
            {
                if (precond.value().is_boolean())
                {
                    WorldProperty wp;
                    wp.value = precond.value();
                    preconditions.AddProperty(precond.key(), wp);
                }
                else
                {
                    std::cout << "A non boolean value has been found in the json !" << std::endl;
                    std::cout << "This simple goap project does not support other types for the moment !" << std::endl;
                }
            }

            // Find out postconditions
            auto postconditions = WorldState();
            for (const auto& postcond : action.value()["postconditions"].items())
            {
                if (postcond.value().is_boolean())
                {
                    WorldProperty wp;
                    wp.value = postcond.value();
                    postconditions.AddProperty(postcond.key(), wp);
                }
                else
                {
                    std::cout << "A non boolean value has been found in the json !" << std::endl;
                    std::cout << "This simple goap project does not support other types for the moment !" << std::endl;
                }
            }

            // Find out action type and generate it
            std::string key = action.key();
            std::shared_ptr<GoapAction> goapAction = nullptr;
            if (key == "catch_mouse")
            {
                goapAction = std::make_shared<GoapActionCatchMouse>(agent, key, cost, preconditions, postconditions);
            }
            else if (key == "chase_mouse")
            {
                goapAction = std::make_shared<GoapActionChaseMouse>(agent, key, cost, preconditions, postconditions);
            }
            else if (key == "search_mouse")
            {
                goapAction = std::make_shared<GoapActionSearchMouse>(agent, key, cost, preconditions, postconditions);
            }
            else if (key == "sleep")
            {
                goapAction = std::make_shared<GoapActionSleep>(agent, key, cost, preconditions, postconditions);
            }
            else
            {
                goapAction = std::make_shared<GoapAction>(agent, key, cost, preconditions, postconditions);
            }
            actions.emplace_back(goapAction);
        }
        return actions;
    }

    /// <summary>
    /// Generate goals according to the corresponding json object and return them in a vector
    /// </summary>
    /// <param name="j"> available actions json object </param>
    /// <returns> goals </returns>
    std::vector<std::shared_ptr<goap::GoapGoal>> GenerateGoals(json& j)
    {
        std::vector<std::shared_ptr<goap::GoapGoal>> goals;
        for (const auto& action : j["goals"].items())
        {
            // Find out goal priority
            int priority = action.value()["priority"].get<int>();

            // Find out goal statements
            auto goalStatements = WorldState();
            for (const auto& statement : action.value()["goal"].items())
            {
                if (statement.value().is_boolean())
                {
                    WorldProperty wp;
                    wp.value = statement.value();
                    goalStatements.AddProperty(statement.key(), wp);
                }
                else
                {
                    std::cout << "A non boolean value has been found in the json !" << std::endl;
                    std::cout << "This simple goap project does not support other types for the moment !" << std::endl;
                }
            }

            // Find out action type and generate it
            std::string key = action.key();
            std::shared_ptr<GoapGoal> goapGoal = nullptr;
            if (key.compare("catch_mouse"))
            {
                goapGoal = std::make_shared<GoapGoalCatchMouse>(key, priority, goalStatements);
            }
            else
            {
                goapGoal = std::make_shared<GoapGoal>(key, priority, goalStatements);
            }
            goals.emplace_back(goapGoal);
        }
        return goals;
    }

};