#pragma once

#include <goap\goals\goap_goal.h>

namespace goap
{
	class GoapGoalCatchMouse : public GoapGoal
	{
	public:
		GoapGoalCatchMouse(std::string name, int priority, const WorldState& worldState);
	};
}