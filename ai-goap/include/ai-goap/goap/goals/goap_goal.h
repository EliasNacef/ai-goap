#pragma once

#include <goap\world_state.h>

namespace goap
{

	/// <summary>
	/// GOAP Goal
	/// </summary>
	class GoapGoal
	{
	public:

		GoapGoal(std::string name, int priority, const WorldState& worldState);
		
		/// <summary>
		/// Check if a replan is required
		/// </summary>
		/// <returns> Is a replan required ?</returns>
		virtual bool IsReplanRequired() const;

		/// <summary>
		/// Check if the goal is reached
		/// </summary>
		/// <param name="state"> state to check </param>
		/// <returns> Is the goal reached ? </returns>
		bool IsGoalSatisfied(const WorldState& state) const;

		const WorldState& GetGoal() const;

		int GetPriority() const;

		virtual void Disp() const;

	protected:
		// Goal name
		std::string m_name;
		// Goal priority
		int m_priority;
		// WorldState to reach
		WorldState m_goal;
	};
}