#pragma once

#include <iostream>
#include <goap/graph/goap_node.h>
#include <goap/world_state.h>

namespace goap
{
	class GoapGraph
	{
	public:
		GoapGraph(std::vector<std::shared_ptr<GoapAction >> actions);

		void AddNode(std::shared_ptr<GoapAction> action);

		void BuildEdges();

	private:
		std::vector<GoapNode> m_graph;
	};
}