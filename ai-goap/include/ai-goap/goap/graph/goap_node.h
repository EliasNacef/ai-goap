#pragma once

#include <goap\actions\goap_action.h>
#include <iostream>


namespace goap
{
	class GoapNode
	{

	public:

		/// <summary>
		/// Goap node ctor
		/// </summary>
		/// <param name="worldState">the node's world state</param>
		/// <param name="actions">All actions that the agent can made</param>
		GoapNode(WorldState worldState, std::vector<std::shared_ptr<goap::GoapAction>> actions);

		/// <summary>
		/// default ctor
		/// </summary>
		GoapNode() = default;

		bool operator==(const GoapNode& other)const
		{
			if (m_parent == other.m_parent &&
				m_generatorAction->GetPreconditions() == other.m_generatorAction->GetPreconditions() &&
				m_generatorAction->GetPostconditions() == other.m_generatorAction->GetPostconditions() &&
				m_ws == other.m_ws && m_g == other.m_g && m_h == other.m_h && m_f == other.m_f)
			{
				return true;
			}

			return false;
		}

		/// <summary>
		/// Return the parent pointer
		/// </summary>
		/// <returns>GoapNode* The parent pointer</returns>
		std::shared_ptr<GoapNode> GetParent();

		/// <summary>
		/// Set the parent pointer
		/// </summary>
		/// <param name="parent">The pointer to set</param>
		void SetParent(std::shared_ptr<GoapNode> parent);

		/// <summary>
		/// Return the action that generate the node
		/// </summary>
		/// <returns>std::shared_ptr<GoapAction> The action that generate the node</returns>
		std::shared_ptr<GoapAction> GetGeneratorAction();

		/// <summary>
		/// Set the action that generate the node
		/// </summary>
		/// <param name="action">The action that generate the node</param>
		void SetGeneratorAction(std::shared_ptr<GoapAction> action);

		/// <summary>
		/// Return the node's world state
		/// </summary>
		/// <returns>WorldState The node world state</returns>
		WorldState GetWorldState();

		/// <summary>
		/// Get all the possible actions
		/// </summary>
		/// <returns>std::vector<std::shared_ptr<goap::GoapAction>> The possible actions</returns>
		std::vector<std::shared_ptr<goap::GoapAction>> GetActions();

		/// <summary>
		/// Return the G function value of the node
		/// </summary>
		/// <returns>int The G function value</returns>
		int GetG();

		/// <summary>
		/// Set the G function value of node
		/// </summary>
		/// <param name="g">int the value</param>
		void SetG(int g);

		/// <summary>
		/// Return the heuristic value
		/// </summary>
		/// <returns>int The heuristic value</returns>
		int GetH();

		/// <summary>
		/// Set the heuristic value
		/// </summary>
		/// <param name="h">The value to set</param>
		void SetH(int h);

		/// <summary>
		/// Return the f function value
		/// </summary>
		/// <returns>int the f function value</returns>
		int GetF();

		/// <summary>
		/// Set the f function value
		/// </summary>
		/// <param name="f">The value to set</param>
		void SetF(int f);
		
		/// <summary>
		/// Generate all successors of the node depending on the actions
		/// </summary>
		/// <param name="actions">The list of all action possible for the sucessors</param>
		/// <returns>std::vector<GoapNode> The list of the successors</returns>
		std::vector<std::shared_ptr<GoapNode>> GenerateSuccessors(std::vector<std::shared_ptr<goap::GoapAction>> actions);
		

	private:
		std::shared_ptr<GoapNode> m_parent;
		std::shared_ptr<GoapAction> m_generatorAction;
		WorldState m_ws;
		std::vector<std::shared_ptr<goap::GoapAction>> m_actions;
		int m_g;
		int m_h;
		int m_f;
	};

}

