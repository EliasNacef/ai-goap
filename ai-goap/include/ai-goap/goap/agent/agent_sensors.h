#pragma once

#include <goap/agent/agent_stats.h>
#include <environment/area.h>
#include <environment/environment.h>
#include <goap/world_state.h>

namespace goap
{
	/// <summary>
	/// Sensors for an agent
	/// </summary>
	class AgentSensors
	{
	public:

		/// <summary>
		/// ctor
		/// </summary>
		AgentSensors();

		/// <summary>
		/// Observe environment
		/// Return current knowledge about environment
		/// </summary>
		/// <returns> WorldState observed (Beliefs) </returns>
		WorldState Use(AgentStats stats, const Vector2& position, const Environment& env) const;

	};
}