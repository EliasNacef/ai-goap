#pragma once

#include <iostream>

#include <environment\environment.h>
#include <goap/states/state.h>
#include <goap/actions/goap_action.h>

namespace goap
{
	/// <summary>
	/// Effectors for an agent
	/// </summary>
	class AgentEffectors
	{
	public:
		/// <summary>
		/// ctor
		/// </summary>
		AgentEffectors();

		/// <summary>
		/// Update current action
		/// Update FSM if needed with new action
		/// Tick FSM
		/// </summary>
		bool Use(Environment& env, WorldState& worldState, std::shared_ptr<GoapAction> action);

		void Tick();

		void CancelAnimation();

		bool IsFSMBusy();

	private:
		// FSM
		std::shared_ptr<State> m_currentState;
	};
}