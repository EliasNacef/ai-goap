#pragma once

#include <queue>
#include <unordered_set>
#include <vector>

#include <environment\environment.h>
#include <goap/components/entity.h>
#include <goap/goap_planner.h>
#include <goap/world_state.h>
#include <goap/actions/goap_action.h>
#include <goap/agent/agent_sensors.h>
#include <goap/agent/agent_stats.h>
#include <goap/agent/agent_effectors.h>
#include <goap/goals/goap_goal.h>


namespace goap
{
	/// <summary>
	///  Agent class
	/// </summary>
	class Agent : public Entity
	{
	public:

		// ctors
		Agent(int id, AgentStats stats);
		Agent(const Agent&) = delete;
		Agent(Agent&&) = delete;

		/// <summary>
		/// Return entity type
		/// </summary>
		/// <returns> string type </returns>
		std::string GetType() override;

		/// <summary>
		/// Return char representation of an agent
		/// </summary>
		/// <returns> Char representation </returns>
		char GetSymbol() override;

		void SetGoals(std::vector<std::shared_ptr<GoapGoal>> goal);
		void SetAvailableActions(std::vector<std::shared_ptr<GoapAction>> actions);

		/// <summary>
		/// Update agent BDI one time
		/// </summary>
		void Tick(Environment& env);

		/// <summary>
		/// Use its sensors (Beliefs) :
		/// Observe environment 
		/// Return the world state sensed
		/// </summary>
		WorldState UseSensors(const Environment& env);

		/// <summary>
		/// Use Goap to plan actions with beliefs (Desires) :
		/// Find an action sequence to perform and return it
		/// </summary>
		std::deque<std::shared_ptr<GoapAction>> UseGoap(const WorldState ws);

		/// <summary>
		/// Use effectors to perform the action sequence (Intentions) :
		/// Update current action
		/// Update FSM
		/// </summary>
		void UseEffectors(Environment& env);

		void DispKnowledge() const;
		void DispPlan() const;
		void DispCurrentAction() const;

		void MoveTowards(Vector2 direction);

		void TryCatchMouse()
		{
			m_stats.isCatching = true;
		}

		bool IsCatching() const
		{
			return m_stats.isCatching;
		}

		int GetMaxEnergy() const
		{
			return m_maxEnergy;
		}

		int GetEnergy() const
		{
			return m_stats.energy;
		}

		int SetEnergy(int energy)
		{
			return m_stats.energy = energy;
		}

	private:
		// STATS
		const int m_maxEnergy;
		AgentStats m_stats;

		// BDI
		AgentSensors m_sensors;
		AgentEffectors m_effectors;

		// GOAP
		WorldState m_wsKnowledge;
		GoapPlanner m_planner;
		bool m_isNewPlan;
		std::vector<std::shared_ptr<GoapGoal>> m_goals;
		std::vector<std::shared_ptr<GoapAction>> m_availableActions;
		std::deque<std::shared_ptr<GoapAction>> m_actionSequence;
		std::shared_ptr<GoapAction> m_currentAction;

		/// <summary>
		/// Check the entire plan executing it on a world state copy
		/// </summary>
		/// <param name="worldState"> world state </param>
		/// <param name="actionSequence"> action sequence to execute </param>
		/// <returns> Return true if the plan is satisfying </returns>
		bool IsPlanValid(WorldState worldState, const GoapGoal& goal, std::deque<std::shared_ptr<GoapAction>> actionSequence);
	};
}
