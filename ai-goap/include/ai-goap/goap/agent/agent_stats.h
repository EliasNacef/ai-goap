#pragma once

namespace goap
{
	struct AgentStats
	{
		char symbol;
		int energy;
		int detectionRange;
		bool isCatching;
	};
}