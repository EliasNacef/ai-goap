#pragma once

namespace goap
{
	/// <summary>
	/// Type for a world property
	/// </summary>
	enum WorldPropertyType
	{
		BOOL, INT, FLOAT
	};
}