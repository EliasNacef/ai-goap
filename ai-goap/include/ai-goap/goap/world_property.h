#pragma once

#include <iostream>
#include <goap\components\vector2.h>
#include <Goap\world_property_type.h>

namespace goap
{
	/// <summary>
	/// Define a world state part
	/// </summary>
	struct WorldProperty
	{
		// Property Value
		bool value;
		//union
		//{
		//	bool bValue;
		//	int nValue;
		//	float fValue;
		//};
		// Associated object id
		int subjectID;
		// Property Type
		WorldPropertyType type;
		// Property Position
		Vector2 position;

	};
}
