#pragma once

#include <optional>
#include <unordered_map>

#include <goap/world_property.h>

namespace goap
{
	/// <summary>
	/// Define an entire world state
	/// </summary>
	class WorldState
	{
	public:
		WorldState();

		friend bool operator==(const WorldState& lhs, const WorldState& rhs)
		{
			const auto& lhsProperties = lhs.GetProperties();
			const auto& rhsProperties = rhs.GetProperties();
			if (lhsProperties.size() != rhsProperties.size())
			{
				// Not the same size
				return false;
			}

			for (auto& property : lhsProperties)
			{
				auto it = rhsProperties.find(property.first);
				if (it != rhsProperties.end())
				{
					if (property.second.value != it->second.value || property.second.position != it->second.position)
					{
						return false;
					}
				}
				else 
				{
					// property does not exist
					return false;
				}
			}
			return true;
		}

		friend bool operator!=(const WorldState& lhs, const WorldState& rhs)
		{
			return !(lhs == rhs);
		}

		bool Validate(const WorldState worldState) const {
			const auto& wsProperties = worldState.GetProperties();
			for (const auto& property : wsProperties) {
				auto it = m_properties.find(property.first);
				if (it != m_properties.end())
				{
					if (property.second.value != it->second.value)
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			return true;
		}

		bool ValidatePartially(const WorldState worldState) const {
			const auto& wsProperties = worldState.GetProperties();
			for (const auto& property : m_properties) {
				auto it = wsProperties.find(property.first);
				if (it != wsProperties.end())
				{
					if (property.second.value == it->second.value)
					{
						return true;
					}
				}
			}
			return false;
		}

		/// <summary>
		/// Update a property if it exists
		/// else a new one
		/// </summary>
		/// <param name="name"></param>
		/// <param name="value"></param>
		void AddProperty(std::string name, WorldProperty worldProperty);

		void RemoveProperty(std::string name);

		const std::optional<WorldProperty> GetProperty(std::string name) const;

		const std::unordered_map<std::string, WorldProperty>& GetProperties() const
		{
			return m_properties;
		}

	private:
		//std::unordered_map<std::string, bool> m_properties;

		// In a more complex GOAP :
		// we should use world properties with differents property type 
		// and with associated object ID
		std::unordered_map<std::string, WorldProperty> m_properties;
	};
}